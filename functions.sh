#!/bin/bash

add_deps() {
  apt-get install -y --no-install-recommends "$@" && \
  apt-mark manual "$@"
}

add_build_deps() {
  apt-get install -y --no-install-recommends "$@" && \
  apt-mark auto "$@"
}

build_envs() {
  export TMPDIR="${TMPDIR:-/tmp}"
  export BUILD="${BUILD:-${TMPDIR}/build}"
  export PREFIX="${PREFIX:-/usr/local}"

  export CC="clang-${CLANG_VER:-15}"
  export CXX="clang++-${CLANG_VER:-15}"
  export CFLAGS="-Ofast -march=native"
  export CXXFLAGS="-Ofast -march=native"
}

build_equivs() {
  local pkg="$1"; shift
  add_build_deps equivs
  mkdir -p "$BUILD"
  cd "$BUILD"
  printf "Package: ${pkg}\nVersion: 99:99\nMaintainer: Dummy Package <dummy@debian.org>\nArchitecture: all\nDescription: dummy ${pkg} package\n Do not install ${pkg}\n" >"${pkg}.equivs"
  equivs-build "${pkg}.equivs"
}

prep() {
  apt-get update
  add_build_deps \
    autoconf \
    automake \
    bison \
    build-essential \
    ca-certificates \
    clang-${CLANG_VER:-15} \
    debhelper \
    debhelper-compat \
    dpkg-dev \
    flex \
    lbzip2 \
    make \
    nasm \
    patch \
    pkg-config \
    unzip \
    wget \
    $build_deps
  if [ -n "$deps" ]; then
    add_deps $deps
  fi
  build_envs
  mkdir -p "$BUILD"
}

build_conf() {
  ./configure \
    CC="$CC" CXX="$CXX" CFLAGS="$CFLAGS" \
    --prefix="$PREFIX" \
    --libdir="$PREFIX/lib/`uname -m`-linux-gnu" \
    "$@"
}

build_deb_tar() {
  mkdir -p "$BUILD"
  cd "$BUILD"
  local pkg="$1"; shift
  local url="$1"; shift
  local file="${url##*/}"
  local srcdir="${file%%.t*}"

  wget -N "$url"
  tar -axf "$file"
  cd "$srcdir"
  dpkg-buildpackage -b -uc "$@"
  dpkg --install ../${pkg}_*.deb
}

build_tar() {
  mkdir -p "$BUILD"
  cd "$BUILD"
  local url="$1"; shift
  local file="${url##*/}"
  local srcdir="${file%%.t*}"

  wget -N "$url"
  tar -axf "$file"
  cd "$srcdir"
  build_conf "$@"
  make -j`nproc`
  make install
}

cleanup() {
  apt-get clean -y
  apt-get autoremove --purge -y
  rm -rf /var/lib/apt/lists/*
  if [ -d "$BUILD" ]; then
    rm -rf "$BUILD"
  fi
}
