#!/bin/bash

. functions.sh

set -e -x

build_deps="
  libfontconfig1-dev
  libfreetype6-dev
  libfribidi-dev
  libgraphicsmagick1-dev
  libpng-dev
  libxml2-dev
  "
deps="
  gettext
  libfontconfig1
  libfribidi0
  libgraphicsmagick-q16-3
  libpng16-16
  libxml2
"
fallback_build_deps="
  libdvdnav-dev
  libdvdread-dev
"
fallback_deps="
  libdvdnav4
  libdvdread8
"

if ! pkg-config --exists dvdnav; then
  deps="$deps $fallback_deps"
  build_deps="$build_deps $fallback_build_deps"
fi

prep

cd "$BUILD"
ver="${1:-develop}"
wget -N -O dvdauthor-$ver.zip https://codeload.github.com/jflemer/dvdauthor/zip/$ver
unzip dvdauthor-$ver.zip
cd dvdauthor-$ver
./bootstrap
build_conf \
  --enable-default-video-format=NTSC \
  --with-graphicsmagick
make -C src -j`nproc`
make -C src install

cleanup
