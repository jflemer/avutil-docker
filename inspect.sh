#!/bin/bash

dpkg -l | egrep -i 'dvd|mjpeg|mpeg|avutil|libav'

tools="
  avconv
  dvd+rw-format
  dvdbackup
  dvdunauthor
  extract_mpeg2
  ffmpeg
  genisoimage
  lavinfo
  mkdvdiso
  mpeg2dec
  mpegtranscode
"

for i in $tools; do
  echo "==> $i"
  $i --version
done
