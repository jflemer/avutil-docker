#!/bin/bash


. functions.sh

set -e -x

# Uses: https://packages.debian.org/source/sid/opencv
CV_VER=${CV_VER:-406}

build_deps="
  flite1-dev
  frei0r-plugins-dev
  ladspa-sdk
  libass-dev
  libavc1394-dev
  libbluray-dev
  libbs2b-dev
  libbz2-dev
  libcaca-dev
  libcdio-paranoia-dev
  libdc1394-dev
  libdrm-dev
  libfontconfig1-dev
  libfreetype6-dev
  libfribidi-dev
  libgl1-mesa-dev
  libgme-dev
  libgnutls28-dev
  libgsm1-dev
  libiec61883-dev
  libjack-jackd2-dev
  libleptonica-dev
  liblzma-dev
  libmp3lame-dev
  libmysofa-dev
  libomxil-bellagio-dev
  libopenal-dev
  libopencore-amrnb-dev
  libopencore-amrwb-dev
  libopencv-imgproc-dev
  libopenjp2-7-dev
  libopenmpt-dev
  libopus-dev
  libpulse-dev
  librsvg2-dev
  librubberband-dev
  libsctp-dev
  libsdl2-dev
  libshine-dev
  libsnappy-dev
  libsoxr-dev
  libspeex-dev
  libssh-gcrypt-dev
  libtesseract-dev
  libtheora-dev
  libtwolame-dev
  libv4l-dev
  libva-dev
  libvdpau-dev
  libvo-aacenc-dev
  libvo-amrwbenc-dev
  libvorbis-dev
  libvpx-dev
  libwavpack-dev
  libwebp-dev
  libx264-dev
  libx265-dev
  libxcb-shape0-dev
  libxcb-shm0-dev
  libxcb-xfixes0-dev
  libxml2-dev
  libxv-dev
  libxvidcore-dev
  libxvmc-dev
  libzmq3-dev
  libzvbi-dev
  zlib1g-dev
"
deps="
  libass9
  libavc1394-0
  libbluray2
  libbs2b0
  libcdio-cdda2
  libcdio-paranoia2
  libdc1394-25
  libflite1
  libgl1
  libgme0
  libiec61883-0
  libjack-jackd2-0
  libmysofa1
  libopenal1
  libopencore-amrnb0
  libopencore-amrwb0
  libopencv-core${CV_VER}
  libopencv-imgproc${CV_VER}
  libopenmpt0
  libraw1394-11
  librubberband2
  libsdl2-2.0-0
  libsndio7.0
  libssh-gcrypt-4
  libtesseract5
  libv4l-0
  libvdpau1
  libxvidcore4
  libvo-aacenc0
  libvo-amrwbenc0
  libxcb-shape0
  libxcb-xfixes0
  libxv1
  libzmq5
  vpx-tools
  x265
"

# libcrystalhd-dev

prep

ver="${1:-12.3}"; shift
cd "$BUILD"
wget -N -O libav-$ver.zip https://codeload.github.com/libav/libav/zip/$ver
unzip libav-$ver.zip
cd libav-$ver
#wget -O libav-x264v_153.patch -N 'https://git.libav.org/?p=libav.git;a=patch;h=c6558e8840fbb2386bf8742e4d68dd6e067d262e'
#patch -p1 < libav-x264v_153.patch && rm libav-x264v_153.patch
./configure --cc="$CC" --extra-cflags="$CFLAGS" --prefix="$PREFIX" --libdir="$PREFIX/lib/`uname -m`-linux-gnu" \
  --cpu=native \
  --disable-debug \
  --enable-gpl \
  --enable-version3 \
  --enable-nonfree \
  --disable-shared \
  --enable-frei0r \
  --enable-gnutls \
  --enable-libbs2b \
  --enable-libcdio \
  --enable-libdc1394 \
  --enable-libfontconfig \
  --enable-libfreetype \
  --enable-libgsm \
  --enable-libmp3lame \
  --enable-libopencore-amrnb \
  --enable-libopencore-amrwb \
  --enable-libopus \
  --enable-libpulse \
  --enable-libsnappy \
  --enable-libspeex \
  --enable-libtheora \
  --enable-libtwolame \
  --enable-libvo-aacenc \
  --enable-libvo-amrwbenc \
  --enable-libvorbis \
  --enable-libwavpack \
  --enable-libwebp \
  --enable-libx264 \
  --enable-libx265 \
  --enable-libxvid

#  --enable-libvpx \

make DEPYASM=true -j`nproc` avconv avprobe
make install-progs

cleanup
