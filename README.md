Audio/Video Utilities Container
===============================

Quick access to current A/V tools without dealing with getting them installed
on your system.

Tools
-----

 * cdrdao
 * dvdauthor
 * dvdbackup
 * ffmpeg (tools and libs)
 * genisoimage
 * growisofs
 * liba52 tools
 * libav tools (no libs)
 * libdvdcss
 * libdvdnav
 * libdvdread
 * m2vrequantiser
 * mbuffer
 * mjpegtools
 * mkvtoolnix
 * mpeg2dec
 * sg3-utils
 * vobcopy
 * wodim

Build
-----

    docker pull debian:unstable-slim
    docker build . -t avutil:test
    docker image tag avutil:test avutil:latest

Use
---

Run the image with the media files/devices volume mounted, using the normal
command line you would for the tool.

    docker run --device /dev/sr0 -v /tmp:/video --rm -it \
      dvdbackup -M -i /dev/sr0 -o /video

Or use this image as the base and layer on your own tools/scripts.

    FROM avutil:latest
    COPY my-av-pipeline /usr/bin/
    ENTRYPOINT ["my-av-pipeline"]

Issues
------

 * chromaprint currently disabled (circular deps for ffmpeg)

References
----------

 * https://www.videolan.org/developers/libdvdcss.html
 * https://www.videolan.org/developers/libdvdnav.html
 * https://github.com/jflemer/dvdauthor
 * https://www.ffmpeg.org/download.html#releases
 * https://github.com/libav/libav
 * https://sourceforge.net/projects/mjpeg/files/mjpegtools/
 * https://bitbucket.org/shlomif/transcode-tcforge/downloads/?tab=tags

License
-------

 * BSD-2-Clause

