#!/bin/bash

. functions.sh

set -e -x

apt-get update
add_deps \
  cdrdao \
  coreutils \
  dvdbackup \
  file \
  genisoimage \
  growisofs \
  liba52-0.7.4-dev \
  libxml-libxml-perl \
  m2vrequantiser \
  mbuffer \
  mkvtoolnix \
  mpeg2dec \
  perl-base \
  sg3-utils \
  udftools \
  vobcopy \
  wodim

cleanup
