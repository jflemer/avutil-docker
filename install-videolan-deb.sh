#!/bin/bash

. functions.sh

set -e -x

build_deps=""
deps=""

prep

add_deps libdvdread8 libdvdnav4

add_build_deps libdvd-pkg
dpkg-reconfigure libdvd-pkg

#ver="${1:-1.4.3-1}"; shift
#build_deb_tar libdvdcss2 https://salsa.debian.org/multimedia-team/libdvdcss/-/archive/debian/${ver}/libdvdcss-debian-${ver}.tar.bz2

build_equivs libdvd-pkg

cleanup
