#!/bin/bash

. functions.sh

set -e -x

build_deps="
  graphicsmagick-libmagick-dev-compat
  libdv4-dev
  libfreetype6-dev
  libjpeg-dev
  liblzo2-dev
  libmjpegtools-dev
  libmp3lame-dev
  libmpeg2-4-dev
  libsdl1.2-dev
  libtheora-dev
  libtwolame-dev
  libv4l-dev
  libvorbis-dev
  libx264-dev
  libxml2-dev
  libxvidcore-dev
  "
deps="
  libdv4
  liblavfile-2.1-0
  liblavjpeg-2.1-0
  liblavplay-2.1-0
  liblzo2-2
  libmjpegutils-2.1-0
  libmpeg2-4
  libmpeg2encpp-2.1-0
  libmplex2-2.1-0
  libsdl1.2debian
  libv4l-0
  libxml2
"
fallback_build_deps="
  libdvdnav-dev
  libdvdread-dev
"
fallback_deps="
  libdvdnav4
  libdvdread8
"

if ! pkg-config --exists dvdnav; then
  deps="$deps $fallback_deps"
  build_deps="$build_deps $fallback_build_deps"
fi

prep

ver="${1:-1.1.7}"; shift
build_tar "https://bitbucket.org/france/transcode-tcforge/downloads/transcode-$ver.tar.bz2" \
  --enable-shared \
  --enable-a52 \
  --enable-alsa \
  --enable-ffmpeg \
  --enable-freetype2 \
  --enable-iconv \
  --enable-imagemagick \
  --enable-lame \
  --enable-libavcodec \
  --enable-libavformat \
  --enable-libdv \
  --enable-libdvdread \
  --enable-libjpeg \
  --enable-libmpeg2 \
  --enable-libpostproc \
  --disable-libquicktime \
  --enable-libv4l2 \
  --enable-libv4lconvert \
  --enable-libxml2 \
  --enable-lzo \
  --enable-mjpegtools \
  --enable-ogg \
  --enable-oss \
  --enable-sdl \
  --enable-theora \
  --enable-v4l \
  --enable-vorbis \
  --enable-x264 \
  --enable-xvid

cleanup
