#!/bin/bash

# See: https://www.ffmpeg.org/download.html#releases
# See: https://packages.debian.org/source/sid/ffmpeg

. functions.sh

set -e -x

# Uses: https://packages.debian.org/source/sid/opencv
CV_VER=${CV_VER:-406}

if [ x"$1" = x"APT" ]; then
  deps="
    ffmpeg
    libavcodec-dev
    libavcodec-extra
    libavdevice-dev
    libavfilter-dev
    libavfilter-extra
    libavformat-dev
    libavformat-extra
    libavutil-dev
    libpostproc-dev
    libswresample-dev
    libswscale-dev
  "

  prep
  cleanup
  exit
fi

build_deps="
  flite1-dev
  frei0r-plugins-dev
  ladspa-sdk
  libass-dev
  libavc1394-dev
  libbluray-dev
  libbs2b-dev
  libbz2-dev
  libcaca-dev
  libcdio-paranoia-dev
  libdc1394-dev
  libdrm-dev
  libfontconfig1-dev
  libfreetype6-dev
  libfribidi-dev
  libgl1-mesa-dev
  libgme-dev
  libgnutls28-dev
  libgsm1-dev
  libiec61883-dev
  libjack-jackd2-dev
  libleptonica-dev
  liblzma-dev
  libmp3lame-dev
  libmysofa-dev
  libomxil-bellagio-dev
  libopenal-dev
  libopencore-amrnb-dev
  libopencore-amrwb-dev
  libopencv-core-dev
  libopencv-dev
  libopencv-imgproc-dev
  libopenjp2-7-dev
  libopenmpt-dev
  libopus-dev
  libpulse-dev
  librsvg2-dev
  librubberband-dev
  libsctp-dev
  libsdl2-dev
  libshine-dev
  libsnappy-dev
  libsoxr-dev
  libspeex-dev
  libssh-gcrypt-dev
  libtesseract-dev
  libtheora-dev
  libtwolame-dev
  libv4l-dev
  libva-dev
  libvdpau-dev
  libvo-aacenc-dev
  libvo-amrwbenc-dev
  libvorbis-dev
  libvpx-dev
  libwavpack-dev
  libwebp-dev
  libx264-dev
  libx265-dev
  libxcb-shape0-dev
  libxcb-shm0-dev
  libxcb-xfixes0-dev
  libxml2-dev
  libxv-dev
  libxvidcore-dev
  libxvmc-dev
  libzmq3-dev
  libzvbi-dev
  zlib1g-dev
"
deps="
  libass9
  libavc1394-0
  libbluray2
  libbs2b0
  libcdio-cdda2
  libcdio-paranoia2
  libdc1394-25
  libflite1
  libgl1
  libgme0
  libgsm1
  libiec61883-0
  libjack-jackd2-0
  libmysofa1
  libopenal1
  libopencore-amrnb0
  libopencore-amrwb0
  libopencv-core${CV_VER}
  libopencv-imgproc${CV_VER}
  libopenmpt0
  libraw1394-11
  librsvg2-2
  librubberband2
  libsdl2-2.0-0
  libshine3
  libsnappy1v5
  libsndio7.0
  libsoxr0
  libspeex1
  libssh-gcrypt-4
  libtesseract5
  libtheora0
  libtwolame0
  libv4l-0
  libva-drm2
  libva-x11-2
  libva2
  libvo-aacenc0
  libvo-amrwbenc0
  libx264-164
  libx265-199
  libxcb-shape0
  libxcb-xfixes0
  libxv1
  libzmq5
  libzvbi0
"

prep

# ============================================================================

ver="${1:-6.0}"; shift
mkdir -p "$BUILD"
cd "$BUILD"
wget -N https://www.ffmpeg.org/releases/ffmpeg-$ver.tar.bz2
tar -axf ffmpeg-$ver.tar.bz2
cd ffmpeg-$ver
./configure \
  --cc="$CC" \
  --cxx="$CXX" \
  --extra-cflags="$CFLAGS" \
  --extra-cxxflags="$CXXFLAGS" \
  --prefix="$PREFIX" \
  --libdir="$PREFIX/lib/`uname -m`-linux-gnu" \
  --cpu=native \
  --disable-debug \
  --enable-gpl \
  --enable-version3 \
  --enable-nonfree \
  --enable-shared \
  --disable-htmlpages \
  --disable-podpages \
  --disable-chromaprint \
  --enable-frei0r \
  --enable-gnutls \
  --enable-ladspa \
  --enable-libass \
  --enable-libbluray \
  --enable-libbs2b \
  --enable-libcaca \
  --enable-libcdio \
  --enable-libdc1394 \
  --enable-libdrm \
  --enable-libflite \
  --enable-libfontconfig \
  --enable-libfreetype \
  --enable-libfribidi \
  --enable-libgme \
  --enable-libgsm \
  --enable-libiec61883 \
  --enable-libmp3lame \
  --enable-libmysofa \
  --enable-libopencore_amrnb \
  --enable-libopencore_amrwb \
  --enable-libopenjpeg \
  --enable-libopenmpt \
  --enable-libopus \
  --enable-libpulse \
  --enable-librsvg \
  --enable-librubberband \
  --enable-libshine \
  --enable-libsnappy \
  --enable-libsoxr \
  --enable-libspeex \
  --enable-libssh \
  --enable-libtesseract \
  --enable-libtheora \
  --enable-libtwolame \
  --enable-libv4l2 \
  --enable-libvo_amrwbenc \
  --enable-libvorbis \
  --enable-libvpx \
  --enable-libwebp \
  --enable-libx264 \
  --enable-libx265 \
  --enable-libxml2 \
  --enable-libxvid \
  --enable-libzmq \
  --enable-libzvbi \
  --enable-omx \
  --enable-openal \
  --enable-opengl \
  --enable-sdl2 \
  --enable-vaapi

# ffmpeg does not support opencv 4.x?
#  --enable-libopencv \

make -j`nproc`
make install

# ============================================================================

cleanup
