#!/bin/bash

. functions.sh

set -e -x

build_deps=""
deps=""

prep

ver="${1:-1.4.3}"; shift
build_tar "https://download.videolan.org/pub/videolan/libdvdcss/$ver/libdvdcss-$ver.tar.bz2" \
  --disable-doc

ver="${1:-6.1.1}"; shift
if [ x"$ver" = x"APT" ]; then
  add_deps libdvdread8
else
  build_tar "https://download.videolan.org/pub/videolan/libdvdread/$ver/libdvdread-$ver.tar.bz2"
    --disable-apidoc
fi

ver="${1:-6.1.3}"; shift
if [ x"$ver" = x"APT" ]; then
  add_deps libdvdnav4
else
  build_tar "https://download.videolan.org/pub/videolan/libdvdnav/$ver/libdvdnav-$ver.tar.bz2"
fi

cleanup
