#!/bin/bash

. functions.sh

set -e -x

build_deps="
  libdv4-dev
  libgtk2.0-dev
  libjpeg-dev
  libpng-dev
  libsdl1.2-dev
"
deps="
  libdv4
  libjpeg62-turbo
  libpng16-16
  libsdl1.2debian
  zlib1g
"

prep

ver="${1:-2.2.1}"; shift
build_tar "https://downloads.sourceforge.net/project/mjpeg/mjpegtools/$ver/mjpegtools-$ver.tar.gz" \
  --without-libquicktime \
  --with-libdv \
  --with-libpng \
  --with-dga \
  --with-gtk \
  --with-libsdl \
  --with-sdlgfx

cleanup
