FROM debian:unstable-slim as build
#FROM debian:testing-slim

ENV LC_ALL=C.UTF-8

WORKDIR /tmp

COPY functions.sh /tmp/

RUN sed -i -e 's/^Components:.*/Components: main contrib/' /etc/apt/sources.list.d/debian.sources

COPY install-tools.sh /tmp/
RUN bash -x ./install-tools.sh

ENV CLANG_VER=15

# Uses: https://packages.debian.org/source/sid/opencv
ENV CV_VER=406

# 1: libdvdcss  https://www.videolan.org/developers/libdvdcss.html
# 2: libdvdread https://www.videolan.org/developers/libdvdnav.html
# 3: libdvdnav  https://www.videolan.org/developers/libdvdnav.html
#COPY install-videolan-libs.sh /tmp/
#RUN bash -x ./install-videolan-libs.sh 1.4.3 6.1.3 6.1.1

# 1: libdvdcss  https://salsa.debian.org/multimedia-team/libdvdcss/-/tags
COPY install-videolan-deb.sh /tmp/
RUN bash -x ./install-videolan-deb.sh 1.4.3-1

# 1: dvdauthor  https://github.com/jflemer/dvdauthor/commits/develop
COPY install-dvdauthor.sh /tmp/
RUN bash -x ./install-dvdauthor.sh b4d96ca971b24149a18f0acdc19eac268716fac5

# 1: ffmpeg     https://www.ffmpeg.org/download.html#releases
COPY install-ffmpeg.sh /tmp/
RUN bash -x ./install-ffmpeg.sh 6.0

# 1: libav      https://github.com/libav/libav
COPY install-libav.sh /tmp/
RUN bash -x ./install-libav.sh c4642788e83b0858bca449f9b6e71ddb015dfa5d

# 1: https://sourceforge.net/projects/mjpeg/files/mjpegtools/
COPY install-mjpegtools.sh /tmp/
RUN bash -x ./install-mjpegtools.sh 2.2.1

# 1: https://bitbucket.org/shlomif/transcode-tcforge/downloads/?tab=tags
#COPY install-transcode.sh /tmp/
#RUN bash -x ./install-transcode.sh 1.1.7

COPY inspect.sh /tmp/
RUN bash -x ./inspect.sh | tee /tmp/inspect.txt

RUN apt-mark showmanual > /tmp/packages.txt


FROM debian:unstable-slim

COPY --from=build /usr/local/bin /usr/local/bin
COPY --from=build /usr/local/lib /usr/local/lib
COPY --from=build /usr/local/share /usr/local/share
COPY --from=build /usr/src/*/libdvdcss*.deb /tmp/
COPY --from=build /tmp/*.deb /tmp/
COPY --from=build /tmp/*.txt /tmp/

RUN \
  sed -i -e 's/^Components:.*/Components: main contrib/' /etc/apt/sources.list.d/debian.sources && \
  apt-get update && \
  apt-get install -y --no-install-recommends $(grep -v libdvdcss /tmp/packages.txt) && \
  apt-get clean -y && \
  apt-get autoremove --purge -y && \
  rm -rf /var/lib/apt/lists/*

RUN dpkg --install /tmp/libdvd-pkg*.deb /tmp/libdvdcss?_*.deb

WORKDIR /tmp
